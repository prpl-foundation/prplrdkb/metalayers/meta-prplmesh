SRC_URI = "git://gitlab.com/prpl-foundation/prplmesh/pwhm/libraries/libswlc.git;protocol=https;nobranch=1;"

SRCREV = "v5.20.3"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "libswlc"
LICENSE = "BSD-2-Clause-Patent.txt"
LIC_FILES_CHKSUM = "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "libswlc"

DEPENDS = "openssl libsahtrace"

FILES_SOLIBSDEV = ""

FILES_${PN}-dev += "${includedir}/swl/*.h"
FILES_${PN} += "${libdir}/${COMPONENT}${SOLIBS}"
FILES_${PN} += "${libdir}/${COMPONENT}${SOLIBSDEV}"

INSANE_SKIP:${PN} += "dev-so"