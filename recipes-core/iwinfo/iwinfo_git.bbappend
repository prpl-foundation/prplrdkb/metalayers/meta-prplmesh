# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: 2020 the prplMesh contributors (see AUTHORS.md)
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.

SRC_URI_remove = "file://0001-Makefile-LDFLAGS-set-liblua5.1-for-lua-lib.patch"
