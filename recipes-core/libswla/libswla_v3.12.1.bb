SRC_URI = "git://gitlab.com/prpl-foundation/prplmesh/pwhm/libraries/libswla.git;protocol=https;nobranch=1;"

SRCREV = "v3.12.1"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "libswla"
LICENSE = "BSD-2-Clause-Patent.txt"
LIC_FILES_CHKSUM = "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "libswla"

DEPENDS = "libswlc libsahtrace libamxc libamxp libamxd libamxb"

FILES_SOLIBSDEV = ""

FILES_${PN}-dev += "${includedir}/swla/*.h"
FILES_${PN} += "${libdir}/${COMPONENT}${SOLIBS}"
FILES_${PN} += "${libdir}/${COMPONENT}${SOLIBSDEV}"

INSANE_SKIP:${PN} += "dev-so"