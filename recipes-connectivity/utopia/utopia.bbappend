# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: 2020 the prplMesh contributors (see AUTHORS.md)
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.

# This bbappend will be ignored according BBMASK located in "conf/layer.conf"
# for any machine besides Puma7

FILESEXTRAPATHS_prepend := "${THISDIR}:"

FILES_${PN} += "/opt/prplmesh/scripts/dhcp_notify_event.sh "

SRC_URI_append += " file://dhcp_notify_event.sh "

# file://add_dhcp_script_option.patch was already applyed

do_install_append() {
    install -d ${D}/opt/prplmesh/scripts/
    install -D -m 0755 ${WORKDIR}/dhcp_notify_event.sh ${D}/opt/prplmesh/scripts/
}
