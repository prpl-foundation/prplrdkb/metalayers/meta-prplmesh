#!/bin/sh

event_name="$1"
mac="$2"
ip_addr="$3"
if [ "$4" == "" ]; then
	hostname="Unknown"
else
	hostname="$4"
fi

# send DHCP event to Beerocks module for managing IRE's/STA's
ubus call dhcp_event dhcp_event '{
	"op":"'$event_name'",
	"mac":"'$mac'",
	"ip":"'$ip_addr'",
	"hostname":"'$hostname'"
}'


