# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: 2020 the prplMesh contributors (see AUTHORS.md)
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

#
# hostapd bbapend in "meta-cmf-raspberrypi" add systemd unit and startup script for hostapd
# Startup script analyze hostapd configs in /nvram folder
#   but the same bbappend install that files into folder /usr/ccsp/wifi/
#   and no one copy/move config into /nvram
# Because of that hostapd cannot start
# Next patch add into hostapd_start.sh the step of creation in /nvram folder 
#   the symlinks to configs in /usr/ccsp/wifi folder  
#
SRC_URI_append_raspberrypi-rdk-broadband += "file://create-hostapd-conf-symlinks-in-nvram.patch;patchdir=${WORKDIR};striplevel=1"

SRC_URI_append_turris += " \
	file://020-ignore-4addr-mode-enabling-error.patch \
	file://110-notify-mgmt-frames.patch \
	file://120-reconfigure-wps-credentials.patch \
	file://300-noscan.patch \
	file://360-ctrl_iface_reload.patch \
	file://370-ap_sta_support.patch \
"
