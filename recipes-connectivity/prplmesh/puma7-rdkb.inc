# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: 2020 the prplMesh contributors (see AUTHORS.md)
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.
#
# Puma7 RDKB specific
#
TARGET_PLATFORM_puma7 ?= "rdkb"
BWL_TYPE_puma7 ?= "bwl-dwpal"

BEEROCKS_BRIDGE_IFACE_puma7 ?= "brlan0"
BEEROCKS_BH_WIRE_IFACE_puma7 ?= "eth0"

SRC_URI_append_puma7 ?= " \
    file://prplmesh-syslog-ng.conf \
"

DEPENDS += "bridge-utils"
CFLAGS_append = " -Wno-implicit-fallthrough"

FILES_${PN}_append_puma7 ?= "\
    ${sysconfdir}/syslog-ng/scl/prplmesh \
"

do_install_machine_specific_puma7() {
    install -d ${D}/${sysconfdir}/syslog-ng/scl/prplmesh
    install -D -m 0644 ${WORKDIR}/prplmesh-syslog-ng.conf ${D}/${sysconfdir}/syslog-ng/scl/prplmesh/prplmesh.conf
}

