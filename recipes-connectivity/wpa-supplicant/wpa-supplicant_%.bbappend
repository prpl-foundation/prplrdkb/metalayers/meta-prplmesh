# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: 2021 the prplMesh contributors (see AUTHORS.md)
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI_append_turris += " \
	file://020-ignore-4addr-mode-enabling-error.patch \
	file://110-notify-mgmt-frames.patch \
	file://120-reconfigure-wps-credentials.patch \
	file://300-noscan.patch \
	file://360-ctrl_iface_reload.patch \
	file://370-ap_sta_support.patch \
"
