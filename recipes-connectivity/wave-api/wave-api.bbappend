# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: 2020 the prplMesh contributors (see AUTHORS.md)
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.

# This bbappend will be ignored according BBMASK located in "conf/layer.conf"
# for any machine besides Puma7
PACKAGECONFIG_append = " prplmesh"
